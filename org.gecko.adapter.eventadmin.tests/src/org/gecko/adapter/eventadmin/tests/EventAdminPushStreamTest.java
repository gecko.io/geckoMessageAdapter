/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.adapter.eventadmin.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.gecko.core.tests.AbstractOSGiTest;
import org.gecko.core.tests.ServiceChecker;
import org.gecko.osgi.messaging.Message;
import org.gecko.osgi.messaging.annotations.RequireEventAdminMessageAdapter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceObjects;
import org.osgi.service.cm.Configuration;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.event.EventHandler;
import org.osgi.util.promise.Promise;
import org.osgi.util.pushstream.PushStream;

@RequireEventAdminMessageAdapter
@RunWith(MockitoJUnitRunner.class)
public class EventAdminPushStreamTest extends AbstractOSGiTest{

	/**
	 * Creates a new instance.
	 */
	public EventAdminPushStreamTest() {
		super(FrameworkUtil.getBundle(EventAdminPushStreamTest.class).getBundleContext());
	}
	
//	@Test
	public void test() {
		
	}
	
	
	@Test
	public void basicTest() throws Exception {
		ExecutorService es = Executors.newCachedThreadPool();
		
		Dictionary<String, Object> props = new Hashtable<>();
		
		props.put("topic", "test/topic");
		
		ServiceChecker<PushStream> createCheckerTrackedForCleanUp = createTrackedChecker(PushStream.class);
		createCheckerTrackedForCleanUp.start();
		
		
		Configuration createConfigForCleanup = createConfigForCleanup("EventAdminTopicSubscription", "?", props);

		createCheckerTrackedForCleanUp.assertCreations(1, true);

//		PushStream<Message> subscribe = createCheckerTrackedForCleanUp.getTrackedService();
		PushStream<Message> subscribe = createCheckerTrackedForCleanUp.assertCreations(1, true).getTrackedService();
		
		subscribe.onError(t -> {
			System.err.println(String.format("[%s] ERROR BUFFER ", System.currentTimeMillis()));
			System.err.println(t.getMessage());
			t.printStackTrace();
		});
		
		int messages = 2000;
		
		CountDownLatch latch = new CountDownLatch(messages);
		
		Promise<Void> promise = subscribe.adjustBackPressure((m,bp)->{
			System.out.println("bp: " + bp);
			return bp;
		}).fork(5, 0, es).forEach(m -> {
//			es.submit(() -> {
				
				String message = new String(m.payload().array());
				assertTrue(message.startsWith("test"));
				latch.countDown();
				System.out.println("sub content: " + message + " ts: " + System.currentTimeMillis());
//			});
		});

		EventAdmin ea = getService(EventAdmin.class);
		
		Executors.newSingleThreadExecutor().execute(() -> {
			System.err.println(String.format("[%s] START POSTING ", System.currentTimeMillis()));
			for(int i = 0; i < messages; i++) {
				try {
					Event event = new Event("test/topic", Collections.singletonMap("content", ByteBuffer.wrap(("test" + i).getBytes())));
					ea.postEvent(event);
//					Thread.sleep(10);
				} catch (Exception e) {
					assertNull(e);
				}
			}
			System.err.println(String.format("[%s] FINISHED POSTING ", System.currentTimeMillis()));
		});

		assertTrue("Not all messages have been prcessed. Current count " + latch.getCount(), latch.await(10, TimeUnit.SECONDS));

		ServiceChecker<EventHandler> eventHandlerTracker = createTrackedChecker(EventHandler.class);
		
		eventHandlerTracker.start();
		subscribe.close();
		eventHandlerTracker.assertRemovals(0, true);
		
		deleteConfigurationAndRemoveFromCleanup(createConfigForCleanup);
		eventHandlerTracker.assertRemovals(1, true);
	}

	@Test
	public void basicTestMultipleStreams() throws Exception {
		ExecutorService es = Executors.newCachedThreadPool();
		
		Dictionary<String, Object> props = new Hashtable<>();
		
		props.put("topic", "test/topic");
		
		ServiceChecker<PushStream> createCheckerTrackedForCleanUp = createTrackedChecker(PushStream.class);
		createCheckerTrackedForCleanUp.start();
		
		
		Configuration createConfigForCleanup = createConfigForCleanup("EventAdminTopicSubscription", "?", props);
		
		createCheckerTrackedForCleanUp.assertCreations(1, true);
		
//		PushStream<Message> subscribe = createCheckerTrackedForCleanUp.getTrackedService();
		ServiceObjects<PushStream> serviceObjects = getServiceObjects(PushStream.class);
		PushStream<Message> subscribe1 = serviceObjects.getService();
		PushStream<Message> subscribe2 = serviceObjects.getService();
		PushStream<Message> subscribe3 = serviceObjects.getService();
		PushStream<Message> subscribe4 = serviceObjects.getService();
		PushStream<Message> subscribe5 = serviceObjects.getService();
		PushStream<Message> subscribe6 = serviceObjects.getService();
		
		subscribe1.onError(t -> {
			System.err.println(t.getMessage());
			t.printStackTrace();
		});
		subscribe2.onError(t -> {
			System.err.println(t.getMessage());
			t.printStackTrace();
		});
		subscribe3.onError(t -> {
			System.err.println(t.getMessage());
			t.printStackTrace();
		});
		subscribe4.onError(t -> {
			System.err.println(t.getMessage());
			t.printStackTrace();
		});
		subscribe5.onError(t -> {
			System.err.println(t.getMessage());
			t.printStackTrace();
		});
		subscribe6.onError(t -> {
			System.err.println(t.getMessage());
			t.printStackTrace();
		});
		
		assertNotEquals(subscribe1, subscribe2);
		
		int messages = 100;
		
		CountDownLatch latch1 = new CountDownLatch(messages);
		CountDownLatch latch2 = new CountDownLatch(messages);
		CountDownLatch latch3 = new CountDownLatch(messages);
		CountDownLatch latch4 = new CountDownLatch(messages);
		CountDownLatch latch5 = new CountDownLatch(messages);
		CountDownLatch latch6 = new CountDownLatch(messages);
		
		CountDownLatch closeCounter = new CountDownLatch(6);
		
		subscribe1.onClose(() -> closeCounter.countDown());
		subscribe2.onClose(() -> closeCounter.countDown());
		subscribe3.onClose(() -> closeCounter.countDown());
		subscribe4.onClose(() -> closeCounter.countDown());
		subscribe5.onClose(() -> closeCounter.countDown());
		subscribe6.onClose(() -> closeCounter.countDown());
		
		subscribe1.forEach(m -> {
			String message = new String(m.payload().array());
			assertTrue(message.startsWith("test"));
			latch1.countDown();
			System.out.println("sub content: " + message + " ts: " + System.currentTimeMillis());
		});

		subscribe2.forEach(m -> {
			String message = new String(m.payload().array());
			assertTrue(message.startsWith("test"));
			latch2.countDown();
			System.out.println("sub content: " + message + " ts: " + System.currentTimeMillis());
		});
		subscribe3.forEach(m -> {
			String message = new String(m.payload().array());
			assertTrue(message.startsWith("test"));
			latch3.countDown();
			System.out.println("sub content: " + message + " ts: " + System.currentTimeMillis());
		});
		subscribe4.forEach(m -> {
			String message = new String(m.payload().array());
			assertTrue(message.startsWith("test"));
			latch4.countDown();
			System.out.println("sub content: " + message + " ts: " + System.currentTimeMillis());
		});
		subscribe5.forEach(m -> {
			String message = new String(m.payload().array());
			assertTrue(message.startsWith("test"));
			latch5.countDown();
			System.out.println("sub content: " + message + " ts: " + System.currentTimeMillis());
		});
		subscribe6.forEach(m -> {
			String message = new String(m.payload().array());
			assertTrue(message.startsWith("test"));
			latch6.countDown();
			System.out.println("sub content: " + message + " ts: " + System.currentTimeMillis());
		});
		
		EventAdmin ea = getService(EventAdmin.class);
		
		Executors.newSingleThreadExecutor().execute(() -> {
			for(int i = 0; i < messages; i++) {
				try {
					Event event = new Event("test/topic", Collections.singletonMap("content", ByteBuffer.wrap(("test" + i).getBytes())));
					ea.postEvent(event);
//					Thread.sleep(10);
				} catch (Exception e) {
					assertNull(e);
				}
			}
		});
		
		assertTrue("Not all messages have been prcessed. Current count " + latch1.getCount(), latch1.await(10, TimeUnit.SECONDS));
		assertTrue("Not all messages have been prcessed. Current count " + latch2.getCount(), latch2.await(10, TimeUnit.SECONDS));
		assertTrue("Not all messages have been prcessed. Current count " + latch3.getCount(), latch3.await(10, TimeUnit.SECONDS));
		assertTrue("Not all messages have been prcessed. Current count " + latch4.getCount(), latch4.await(10, TimeUnit.SECONDS));
		assertTrue("Not all messages have been prcessed. Current count " + latch5.getCount(), latch5.await(10, TimeUnit.SECONDS));
		assertTrue("Not all messages have been prcessed. Current count " + latch6.getCount(), latch6.await(10, TimeUnit.SECONDS));
		
		ServiceChecker<EventHandler> eventHandlerTracker = createTrackedChecker(EventHandler.class);
		
		eventHandlerTracker.start();
		subscribe1.close();
		eventHandlerTracker.assertRemovals(0, true);

		assertEquals(5, closeCounter.getCount());

		subscribe2.close();
		eventHandlerTracker.assertRemovals(0, true);
		assertEquals(4, closeCounter.getCount());
		
		
		deleteConfigurationAndRemoveFromCleanup(createConfigForCleanup);
		eventHandlerTracker.assertRemovals(1, true);
		assertTrue(closeCounter.await(1, TimeUnit.SECONDS));
	}

	@Test
	public void basicTestMultipleStreamsWithError() throws Exception {
		ExecutorService es = Executors.newCachedThreadPool();
		
		Dictionary<String, Object> props = new Hashtable<>();
		
		props.put("topic", "test/topic");
		
		ServiceChecker<PushStream> createCheckerTrackedForCleanUp = createTrackedChecker(PushStream.class);
		createCheckerTrackedForCleanUp.start();
		
		
		Configuration createConfigForCleanup = createConfigForCleanup("EventAdminTopicSubscription", "?", props);
		
		createCheckerTrackedForCleanUp.assertCreations(1, true);
		
//		PushStream<Message> subscribe = createCheckerTrackedForCleanUp.getTrackedService();
		ServiceObjects<PushStream> serviceObjects = getServiceObjects(PushStream.class);
		PushStream<Message> subscribe1 = serviceObjects.getService();
		PushStream<Message> subscribe5 = serviceObjects.getService();
		
		assertNotEquals(subscribe1, subscribe5);
		
		int messages = 100;
		
		CountDownLatch latch1 = new CountDownLatch(messages);
		CountDownLatch latch5 = new CountDownLatch(messages);
		
		CountDownLatch closeCounter = new CountDownLatch(2);
		CountDownLatch errorCounter = new CountDownLatch(1);
		
		subscribe1.onClose(() -> closeCounter.countDown());
		subscribe5.onClose(() -> closeCounter.countDown());
		subscribe5.onError((t) -> {
			errorCounter.countDown();
		});
		
		subscribe1.forEach(m -> {
			String message = new String(m.payload().array());
			assertTrue(message.startsWith("test"));
			latch1.countDown();
			System.out.println("sub content: " + message + " ts: " + System.currentTimeMillis());
		});
		
		subscribe5.forEachEvent(e -> {
			Message m = e.getData();
			String message = new String(m.payload().array());
			assertTrue(message.startsWith("test"));
			latch5.countDown();
			System.out.println("sub content: " + message + " ts: " + System.currentTimeMillis());
			if(latch5.getCount() == 95) {
				throw new RuntimeException("ERROR");
			}
			return 0;
		});
		
		EventAdmin ea = getService(EventAdmin.class);
		
		Executors.newSingleThreadExecutor().execute(() -> {
			for(int i = 0; i < messages; i++) {
				try {
					Event event = new Event("test/topic", Collections.singletonMap("content", ByteBuffer.wrap(("test" + i).getBytes())));
					ea.postEvent(event);
//					Thread.sleep(10);
				} catch (Exception e) {
					assertNull(e);
				}
			}
		});
		
		assertTrue("Not all messages have been prcessed. Current count " + latch1.getCount(), latch1.await(10, TimeUnit.SECONDS));
		
		assertEquals(95, latch5.getCount());
		
		ServiceChecker<EventHandler> eventHandlerTracker = createTrackedChecker(EventHandler.class);
		
		assertEquals(1, closeCounter.getCount());
		assertEquals(0, errorCounter.getCount());

		eventHandlerTracker.start();
		subscribe1.close();
		
		eventHandlerTracker.assertRemovals(0, true);
		assertTrue(closeCounter.await(1,  TimeUnit.SECONDS));
		
		
		deleteConfigurationAndRemoveFromCleanup(createConfigForCleanup);
		eventHandlerTracker.assertRemovals(1, true);
	}

 	/* 
	 * (non-Javadoc)
	 * @see org.gecko.util.test.AbstractOSGiTest#doBefore()
	 */
	@Override
	public void doBefore() {
		// TODO Auto-generated method stub
		
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.util.test.AbstractOSGiTest#doAfter()
	 */
	@Override
	public void doAfter() {
		// TODO Auto-generated method stub
		
	}

}