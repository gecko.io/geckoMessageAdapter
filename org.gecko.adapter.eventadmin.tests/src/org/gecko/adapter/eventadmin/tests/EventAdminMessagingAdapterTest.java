/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.adapter.eventadmin.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.gecko.adapter.eventadmin.context.EventAdminMessagingContext;
import org.gecko.adapter.eventadmin.context.EventAdminMessagingContextBuilder;
import org.gecko.core.tests.AbstractOSGiTest;
import org.gecko.core.tests.ServiceChecker;
import org.gecko.osgi.messaging.Message;
import org.gecko.osgi.messaging.MessagingContext;
import org.gecko.osgi.messaging.MessagingService;
import org.gecko.osgi.messaging.SimpleMessagingContextBuilder;
import org.gecko.osgi.messaging.annotations.RequireEventAdminMessageAdapter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventAdmin;
import org.osgi.service.event.EventHandler;
import org.osgi.util.promise.Promise;
import org.osgi.util.pushstream.PushStream;

@RequireEventAdminMessageAdapter
@RunWith(MockitoJUnitRunner.class)
public class EventAdminMessagingAdapterTest extends AbstractOSGiTest {

	/**
	 * Creates a new instance.
	 */
	public EventAdminMessagingAdapterTest() {
		super(FrameworkUtil.getBundle(EventAdminMessagingAdapterTest.class).getBundleContext());
	}
	
	@Test
	public void test() {
		
	}
	
	@Test
	public void basicTest() throws Exception {
		ExecutorService es = Executors.newCachedThreadPool();
		MessagingService messagingService = getService(MessagingService.class);
		
		PushStream<Message> subscribe = messagingService.subscribe("test/topic");
		
		int messages = 100;
		
		CountDownLatch latch = new CountDownLatch(messages);
		
		subscribe.onError(t -> {
			System.err.println(t.getMessage());
			t.printStackTrace();
		});
		
		Promise<Void> promise = subscribe.adjustBackPressure((m,bp)->{
			System.out.println("bp: " + bp);
			return bp;
		}).fork(5, 0, es).forEach(m -> {
//			es.submit(() -> {
				
				String message = new String(m.payload().array());
				assertTrue(message.startsWith("test"));
				latch.countDown();
				System.out.println("sub content: " + message + " ts: " + System.currentTimeMillis());
//			});
		});

		EventAdmin ea = getService(EventAdmin.class);
		
		
		
		Executors.newSingleThreadExecutor().execute(() -> {
			for(int i = 0; i < messages; i++) {
				try {
					Event event = new Event("test/topic", Collections.singletonMap("content", ByteBuffer.wrap(("test" + i).getBytes())));
					ea.postEvent(event);
//					Thread.sleep(10);
				} catch (Exception e) {
					assertNull(e);
				}
			}
		});
		
		assertTrue("Not all messages have been prcessed. Current count " + latch.getCount(), latch.await(10, TimeUnit.SECONDS));
	}

	@Test
	public void basicTestSendViaMessageAdapter() throws Exception {
		ExecutorService es = Executors.newCachedThreadPool();
		MessagingService messagingService = getService(MessagingService.class);
		
		PushStream<Message> subscribe = messagingService.subscribe("test/topic");
		
		int messages = 100;
		
		CountDownLatch latch = new CountDownLatch(messages);
		
		subscribe.onError(t -> {
			System.err.println(t.getMessage());
			t.printStackTrace();
		});
		
		Promise<Void> promise = subscribe.adjustBackPressure((m,bp)->{
			System.out.println("bp: " + bp);
			return bp;
		}).fork(5, 0, es).forEach(m -> {
			String message = new String(m.payload().array());
			assertTrue(message.startsWith("test"));
			latch.countDown();
			System.out.println("sub content: " + message + " ts: " + System.currentTimeMillis());
		});
		
		EventAdmin ea = getService(EventAdmin.class);
		
		Executors.newSingleThreadExecutor().execute(() -> {
			for(int i = 0; i < messages; i++) {
				try {
//					Event event = new Event("test/topic", Collections.singletonMap("content", ByteBuffer.wrap(("test" + i).getBytes())));
//					ea.postEvent(event);
//					Thread.sleep(10);
					messagingService.publish("test/topic", ByteBuffer.wrap(("test" + i).getBytes()));
				} catch (Exception e) {
					assertNull(e);
				}
			}
		});
		
		assertTrue("Not all messages have been prcessed. Current count " + latch.getCount(), latch.await(10, TimeUnit.SECONDS));
	}
	
	@Test
	public void basicTestSendViaMessageAdapterWithContext() throws Exception {
		ExecutorService es = Executors.newCachedThreadPool();
		MessagingService messagingService = getService(MessagingService.class);
		
		PushStream<Message> subscribe = messagingService.subscribe("test/topic");
		
		int messages = 100;
		
		CountDownLatch latch = new CountDownLatch(messages);
		
		subscribe.onError(t -> {
			System.err.println(t.getMessage());
			t.printStackTrace();
		});
		
		Promise<Void> promise = subscribe.forEach(m -> {
			String message = new String(m.payload().array());
			assertTrue(message.startsWith("test"));
			String contentType = m.getContext().getContentType();
			assertEquals("test", contentType);
			assertTrue(m.getContext() instanceof EventAdminMessagingContext);
			assertEquals("testheader", ((EventAdminMessagingContext) m.getContext()).getHeaders().get("testheader"));
			latch.countDown();
			System.out.println("sub content: " + message + " ts: " + System.currentTimeMillis());
		});
		
		EventAdmin ea = getService(EventAdmin.class);
		
		MessagingContext build = EventAdminMessagingContextBuilder.builder().header("testheader", "testheader").contentType("test").build();
		
		Executors.newSingleThreadExecutor().execute(() -> {
			for(int i = 0; i < messages; i++) {
				try {
//					Event event = new Event("test/topic", Collections.singletonMap("content", ByteBuffer.wrap(("test" + i).getBytes())));
//					ea.postEvent(event);
//					Thread.sleep(10);
					messagingService.publish("test/topic", ByteBuffer.wrap(("test" + i).getBytes()),  build);
				} catch (Exception e) {
					assertNull(e);
				}
			}
		});
		
		assertTrue("Not all messages have been prcessed. Current count " + latch.getCount(), latch.await(10, TimeUnit.SECONDS));
	}

	@Test
	public void basicBackpressure() throws Exception {
		ExecutorService es = Executors.newCachedThreadPool();
		MessagingService messagingService = getService(MessagingService.class);
		
		PushStream<Message> subscribe = messagingService.subscribe("test/topic");
		
		int messages = 2;
		
		CountDownLatch latch = new CountDownLatch(messages);
		
		subscribe.onError(t -> {
			System.err.println(t.getMessage());
			t.printStackTrace();
		});
		
		Promise<Void> promise = subscribe.adjustBackPressure((m,bp)->{
			System.out.println("bp: " + bp);
			return 1000 * 5;
		}).fork(5, 0, es).forEach(m -> {
			String message = new String(m.payload().array());
			assertTrue(message.startsWith("test"));
			latch.countDown();
			System.out.println("sub content: " + message + " ts: " + System.currentTimeMillis());
		});
		
		EventAdmin ea = getService(EventAdmin.class);
		
		MessagingContext build = SimpleMessagingContextBuilder.builder().contentType("test").build();
		
		Executors.newSingleThreadExecutor().execute(() -> {
			for(int i = 0; i < messages; i++) {
				try {
					messagingService.publish("test/topic", ByteBuffer.wrap(("test" + i).getBytes()),  build);
				} catch (Exception e) {
					assertNull(e);
				}
			}
		});
		
		assertTrue("Not all messages have been prcessed. Current count " + latch.getCount(), latch.await(15, TimeUnit.SECONDS));
	}
	
	@Test
	public void testCleanup() throws Exception {
		ExecutorService es = Executors.newCachedThreadPool();
		MessagingService messagingService = getService(MessagingService.class);
		
		PushStream<Message> subscribe = messagingService.subscribe("test/topic");
		
		int messages = 100;
		
		CountDownLatch latch = new CountDownLatch(messages);
		
		subscribe.onError(t -> {
			System.err.println(t.getMessage());
			t.printStackTrace();
		});
		
		subscribe.forEach(m -> {
			String message = new String(m.payload().array());
			assertTrue(message.startsWith("test"));
			latch.countDown();
			System.out.println("sub content: " + message + " ts: " + System.currentTimeMillis());
		});
		
		Executors.newSingleThreadExecutor().execute(() -> {
			for(int i = 0; i < messages; i++) {
				try {
					messagingService.publish("test/topic", ByteBuffer.wrap(("test" + i).getBytes()));
				} catch (Exception e) {
					assertNull(e);
				}
			}
		});
		
		assertTrue("Not all messages have been prcessed. Current count " + latch.getCount(), latch.await(10, TimeUnit.SECONDS));
		
		ServiceChecker<EventHandler> eventHandlerTracker = createTrackedChecker(EventHandler.class);
		
		subscribe.close();
		eventHandlerTracker.assertRemovals(0, true);
	}

	@Test
	public void testCleanupMultiple() throws Exception {
		ExecutorService es = Executors.newCachedThreadPool();
		MessagingService messagingService = getService(MessagingService.class);
		
		int messages = 100;

		PushStream<Message> subscribe = messagingService.subscribe("test/topic");
		
		CountDownLatch latch = new CountDownLatch(messages);
		
		subscribe.onError(t -> {
			System.err.println(t.getMessage());
			t.printStackTrace();
		});
		
		subscribe.forEach(m -> {
			String message = new String(m.payload().array());
			assertTrue(message.startsWith("test"));
			latch.countDown();
			System.out.println("sub content: " + message + " ts: " + System.currentTimeMillis());
		});

		PushStream<Message> subscribe2 = messagingService.subscribe("test/topic");
		
		CountDownLatch latch2 = new CountDownLatch(messages);
		
		subscribe2.onError(t -> {
			System.err.println(t.getMessage());
			t.printStackTrace();
		});
		
		subscribe2.forEach(m -> {
			String message = new String(m.payload().array());
			assertTrue(message.startsWith("test"));
			latch2.countDown();
			System.out.println("sub content: " + message + " ts: " + System.currentTimeMillis());
		});

		PushStream<Message> subscribe3 = messagingService.subscribe("test/topic");
		
		CountDownLatch latch3 = new CountDownLatch(messages);
		
		subscribe3.onError(t -> {
			System.err.println(t.getMessage());
			t.printStackTrace();
		});
		
		subscribe3.forEach(m -> {
			String message = new String(m.payload().array());
			assertTrue(message.startsWith("test"));
			latch3.countDown();
			System.out.println("sub content: " + message + " ts: " + System.currentTimeMillis());
		});
		
		Executors.newSingleThreadExecutor().execute(() -> {
			for(int i = 0; i < messages; i++) {
				try {
					messagingService.publish("test/topic", ByteBuffer.wrap(("test" + i).getBytes()));
				} catch (Exception e) {
					assertNull(e);
				}
			}
		});
		
		assertTrue("Not all messages have been processed. Current count " + latch.getCount(), latch.await(10, TimeUnit.SECONDS));
		assertTrue("Not all messages have been processed. Current count " + latch2.getCount(), latch2.await(10, TimeUnit.SECONDS));
		assertTrue("Not all messages have been processed. Current count " + latch3.getCount(), latch3.await(10, TimeUnit.SECONDS));
		
		ServiceChecker<EventHandler> eventHandlerTracker = createTrackedChecker(EventHandler.class);
		
		subscribe.close();
		eventHandlerTracker.assertRemovals(0, true);

		subscribe2.close();
		eventHandlerTracker.assertRemovals(0, true);
		
		subscribe3.close();
		eventHandlerTracker.assertRemovals(1, true);
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.util.test.AbstractOSGiTest#doBefore()
	 */
	@Override
	public void doBefore() {
		// TODO Auto-generated method stub
		
	}

	/* 
	 * (non-Javadoc)
	 * @see org.gecko.util.test.AbstractOSGiTest#doAfter()
	 */
	@Override
	public void doAfter() {
		// TODO Auto-generated method stub
		
	}

}