/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.adapter.ws.tests.server;

import java.util.concurrent.CountDownLatch;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.component.LifeCycle.Listener;
import org.gecko.adapter.ws.tests.servlet.EventServlet;

/**
 * 
 * @author Mark Hoffmann
 * @since 13.10.2017
 */
public class WSServerRunnable implements Runnable {

	private Server server = new Server();
	private CountDownLatch latch;
	private Listener listener;
	
	public WSServerRunnable(Listener listener) {
		this.listener = listener;
	}
	
	/* 
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		try {
	        ServerConnector connector = new ServerConnector(server);
	        connector.setPort(8888);
	        server.addConnector(connector);

	        // Setup the basic application "context" for this application at "/"
	        // This is also known as the handler tree (in jetty speak)
	        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
	        context.setContextPath("/");
	        server.setHandler(context);
	        if (listener != null) {
	        	server.addLifeCycleListener(listener);
	        }
	        
	        // Add a websocket to a specific path spec
	        ServletHolder holderEvents = new ServletHolder("ws-events", EventServlet.class);
	        context.addServlet(holderEvents, "/events/*");
	        latch = new CountDownLatch(1);
	        server.start();
	        server.dump(System.err);
	        System.out.println("Start server");
	        server.join();
	        latch.await();
		} catch (InterruptedException e) {
			if (latch != null) {
				latch.countDown();
			}
		} catch (Exception e) {
			if (latch != null) {
				latch.countDown();
			}
		} finally {
			System.out.println("Stop server");
			try {
				server.stop();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
