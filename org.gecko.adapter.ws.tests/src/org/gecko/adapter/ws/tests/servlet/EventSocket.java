package org.gecko.adapter.ws.tests.servlet;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketAdapter;

public class EventSocket extends WebSocketAdapter {
	
	private AtomicReference<Session> session = new AtomicReference<Session>(null);
	private Timer timer = new Timer("msg-timer");
	private volatile int cnt = 0;
	private TimerTask task = new TimerTask() {
		
		@Override
		public void run() {
			if (session.get() != null) {
				cnt++;
				try {
					session.get().getRemote().sendString("Message-" + cnt);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	};
	
    @Override
    public void onWebSocketConnect(Session sess) {
        super.onWebSocketConnect(sess);
        session.set(sess);
        System.out.println("Socket Connected: " + sess);
    }
    
    @Override
    public void onWebSocketText(String message) {
        super.onWebSocketText(message);
        System.out.println("Received TEXT message: " + message);
        if (message.equalsIgnoreCase("start") && session != null) {
        	System.out.println("Starting timer");
        	timer.schedule(task, 10l, 1500l);
        }
        if (message.equalsIgnoreCase("stop") && session != null) {
        	System.out.println("Stopping timer");
        	timer.cancel();
        }
    }
    
    @Override
    public void onWebSocketClose(int statusCode, String reason)
    {
        super.onWebSocketClose(statusCode,reason);
        System.out.println("Socket Closed: [" + statusCode + "] " + reason);
    }
    
    @Override
    public void onWebSocketError(Throwable cause)
    {
        super.onWebSocketError(cause);
        cause.printStackTrace(System.err);
    }
}