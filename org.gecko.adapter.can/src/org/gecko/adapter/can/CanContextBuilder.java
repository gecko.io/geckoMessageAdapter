/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.adapter.can;

import org.gecko.osgi.messaging.MessagingContext;
import org.gecko.osgi.messaging.SimpleMessagingContextBuilder;

/**
 * Context builder for the CAN context
 * @author Mark Hoffmann
 * @since 16.09.2018
 */
public class CanContextBuilder extends SimpleMessagingContextBuilder {
	
	private CanContext context = new CanContext();
	
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.osgi.messaging.SimpleMessagingContextBuilder#build()
	 */
	@Override
	public MessagingContext build() {
		return buildContext(context);
	}

}
