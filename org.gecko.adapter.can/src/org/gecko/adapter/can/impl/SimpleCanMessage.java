/**
 * Copyright (c) 2012 - 2018 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.adapter.can.impl;

import java.nio.ByteBuffer;
import java.util.Date;

import org.gecko.adapter.can.CanMessage;
import org.gecko.osgi.messaging.SimpleMessage;

/**
 * 
 * @author mark
 * @since 17.09.2018
 */
public class SimpleCanMessage extends SimpleMessage implements CanMessage {

	private final Date timestamp;
	
	/**
	 * Creates a new instance.
	 */
	public SimpleCanMessage(int clientId, ByteBuffer payload, long timestamp) {
		super(Integer.valueOf(clientId).toString(), payload);
		this.timestamp = new Date(timestamp);
	}
	/* 
	 * (non-Javadoc)
	 * @see org.gecko.adapter.can.CanMessage#getTimestamp()
	 */
	@Override
	public Date getTimestamp() {
		return timestamp;
	}

}
