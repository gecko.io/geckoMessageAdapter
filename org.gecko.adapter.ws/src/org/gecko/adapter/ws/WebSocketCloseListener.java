/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.adapter.ws;

/**
 * Listener that will be notified on websocket session close
 * @author Mark Hoffmann
 * @since 11.10.2017
 */
public interface WebSocketCloseListener {
	
	/**
	 * Calle, when a session was closed
	 * @param url the url of the session that was closeds
	 */
	public void onClose(String url);

}
