/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.eclipselabs.ws.messaging.server;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A very simple websocket server
 * @author Mark Hoffmann
 * @since 14.10.2017
 */
public class WebsocketServer {
	
	private final Logger logger = Logger.getLogger("wsServer");
	private final String host;
	private final String context;
	private final int port;
	private ExecutorService executor;
	private CountDownLatch waitLatch;
	private Future<?> serverFuture;
	private boolean running = false;

	public WebsocketServer(String host, int port, String context) {
		this.host = host;
		this.port = port;
		this.context = context;
	}
	
	/**
	 * Returns <code>true</code>, if the server is running
	 * @return <code>true</code>, if the server is running
	 */
	public boolean isRunning() {
		return running;
	}
	
	/**
	 * Starts the server
	 */
	public void startServer() {
		if (isRunning()) {
			logger.info("Server is already running for ws://" + host + ":" + port + context + ", cannot start again");
			return;
		}
		Runnable serverRunnable = new WSServerRunnable(host, port, context);
    	executor = Executors.newSingleThreadExecutor();
    	serverFuture = executor.submit(serverRunnable);
    	running = true;
    	waitLatch = new CountDownLatch(1);
	}
	
	/**
	 * Stops the server
	 */
	public void stopServerAfter(long value, TimeUnit unit) {
		if (!isRunning()) {
			logger.info("Server is not running for ws://" + host + ":" + port + context + ". There is nothing to stop");
			return;
			
		}
		if (value == -1 && unit == null) {
			waitLatch.countDown();
		} else {
			try {
				waitLatch.await(value, unit);
			} catch (InterruptedException e) {
				logger.log(Level.SEVERE, "Detected an interuption during server run, while waiting", e);
			}
		}
		serverFuture.cancel(true);
		running = false;
		executor.shutdownNow();
		executor.shutdown();
	}
	
	/**
	 * Stops the server
	 */
	public void stopServer() {
		stopServerAfter(-1, null);
	}
	
    public static void main(String[] args) throws InterruptedException  {
    	WebsocketServer server = new WebsocketServer("localhost", 8888, "/events/*");
    	server.startServer();
    	server.stopServerAfter(1000, TimeUnit.SECONDS);
    }
    
}