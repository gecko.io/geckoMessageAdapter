/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.eclipselabs.ws.messaging.server;

import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipselabs.ws.messaging.servlet.EventServlet;

/**
 * 
 * @author Mark Hoffmann
 * @since 13.10.2017
 */
public class WSServerRunnable implements Runnable {

	private final Logger logger = Logger.getLogger("wsServerRunnable");
	private final Server server = new Server();
	private final String host;
	private final String contextPath;
	private final int port;
	private CountDownLatch latch;
	
	public WSServerRunnable(String host, int port, String context) {
		this.host = host == null ? "localhost" : host;
		this.port = port <= 0 ? 8888 : port;
		this.contextPath = context;
	}
	/* 
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		try {
	        ServerConnector connector = new ServerConnector(server);
	        connector.setPort(port);
	        if (host != null) {
	        	connector.setHost(host);
	        }
	        server.addConnector(connector);

	        // Setup the basic application "context" for this application at "/"
	        // This is also known as the handler tree (in jetty speak)
	        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
	        context.setContextPath("/");
	        server.setHandler(context);
	        
	        // Add a websocket to a specific path spec
	        ServletHolder holderEvents = new ServletHolder("ws-events", EventServlet.class);
	        context.addServlet(holderEvents, contextPath);
	        latch = new CountDownLatch(1);
	        server.start();
	        logger.info("Started server under ws://" + host + ":" + port + contextPath);
	        server.join();
	        latch.await();
		} catch (InterruptedException e) {
			logger.log(Level.SEVERE, "Detected an interuption", e);
			if (latch != null) {
				latch.countDown();
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Detected an exception during server run", e);
			if (latch != null) {
				latch.countDown();
			}
		} finally {
			logger.log(Level.INFO, "Stopping server under ws://" + host + ":" + port + contextPath);
			try {
				server.stop();
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Detected an exception during server stop", e);
			}
		}
	}

}
