/**
 * Copyright (c) 2012 - 2017 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.eclipselabs.ws.messaging.client;

import java.net.URI;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.eclipselabs.ws.messaging.servlet.EventClientHandler;

/**
 * Very simple websocket client
 * @author Mark Hoffmann
 * @since 14.10.2017
 */
public class WebsocketClient {
	
	private final Logger logger = Logger.getLogger("wsClient");
	private URI connectUri = URI.create("ws://localhost:8888/events/");
	private static WebSocketClient client = new WebSocketClient();
	
	public WebsocketClient(String uri) {
		connectUri = URI.create(uri);
	}
	
    /**
     * Sends the text for the given session
     * @param session the session to send the text with (<code>null</code>, to create a new session)
     * @param text the text to send
     * @return the session
     */
    public Session sendText(Session session, String text) {
    	if (session == null) {
    		session = initializeSession();
    	}
    	try {
    		if (session != null) {
    			session.getRemote().sendString(text);
    		} else {
    			logger.warning("No session available for uri: " + connectUri);
    		}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception occured sending text: '" + text + "' for session " + session, e);
		}
    	return session;
    }
    
    /**
     * Lazy initializes a session
     * @return the session
     */
    private Session initializeSession() {
    	Session session = null;
    	try {
			client.start();
			// The handler that receives events
			EventClientHandler socket = new EventClientHandler();
			Future<Session> fut = client.connect(socket, connectUri);
			// Wait for Connect
			session = fut.get();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception occured initializing a session for uri: " + connectUri, e);
		}
    	return session;
    }

}
