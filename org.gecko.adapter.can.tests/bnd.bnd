# This is the version of JUnit that will be used at build time and run time
junit: org.apache.servicemix.bundles.junit;version="[4.11,5)"

# This is the version of Mockito that will be used at build time and run time
mockito: org.mockito.mockito-core;version="[1.9,2)",\
  org.objenesis;version="[2.1,3)"

# Discover and run all test cases annotated with the @RunWith annotation
Test-Cases: ${classes;CONCRETE;ANNOTATED;org.junit.runner.RunWith}

# Build dependencies 
-buildpath: \
	${junit},\
	${mockito},\
	osgi.annotation;version=7.0,\
	osgi.core;version=7.0,\
	osgi.cmpn;version=7.0,\
	org.osgi.util.pushstream;version=1.0,\
	org.gecko.adapter.can;version=latest,\
	org.gecko.osgi.messaging;version=latest

javac.source: 1.8
javac.target: 1.8

# We need JUnit and Mockito to resolve the test cases at runtime. 
# Other runtime dependencies should be added as necessary
-runbundles.junit: \
	${junit},\
	${mockito}
-runbundles: \
	org.osgi.util.function;version='[1.1.0,1.1.1)',\
	org.osgi.util.promise;version='[1.1.0,1.1.1)',\
	org.apache.felix.configadmin;version='[1.9.10,1.9.11)',\
	org.apache.felix.scr;version='[2.1.14,2.1.15)',\
	org.osgi.util.pushstream;version='[1.0.1,1.0.2)',\
	org.gecko.adapter.can;version='[1.0.4,1.0.5)',\
	org.gecko.core.api;version='[1.0.5,1.0.6)',\
	org.gecko.core.pushstream;version='[1.1.0,1.1.1)',\
	org.gecko.osgi.messaging;version='[3.1.0,3.1.1)'

Bundle-Version: 1.0.3.SNAPSHOT
Private-Package: org.gecko.adapter.can.tests

Bundle-Copyright: Data In Motion Consulting GmbH
Bundle-License: Eclipse Public License v1.0
Bundle-Vendor: Data In Motion Consulting GmbH

# Needed for Mockito's mocking to work
-runsystempackages.objenesis: sun.misc,sun.reflect

# Use Felix by default
-runfw: org.apache.felix.framework;version='[6.0.1,6.0.2]'
-runvm: -ea
Bundle-Name: Gecko Message Adapter for CAN Tests
Bundle-Description: Tests for the CAN bus implementation of the message adapter
-runrequires: \
	bnd.identity;id='org.gecko.adapter.can',\
	osgi.identity;filter:='(&(osgi.identity=org.apache.felix.configadmin)(version>=1.9.0))'
-runee: JavaSE-1.8